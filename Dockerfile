FROM node:boron
# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install --allow-root

COPY . /usr/src/app
EXPOSE 3000
CMD node index.js